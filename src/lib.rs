use std::{collections::HashMap, fmt::Debug, hash::Hash, ops::Deref};

use bevy::{
  ecs::component::Component,
  input::mouse::{MouseMotion, MouseWheel},
  prelude::*,
};

pub struct InputGameEvent;

pub trait InputInterface {
  fn process_input() -> InputGameEvent;
}

#[derive(PartialEq, Eq, Hash, Debug, Clone)]
#[cfg_attr(feature = "serialize", derive(serde::Serialize, serde::Deserialize))]
pub enum InputEvent {
  KeyCode(KeyCode),
  MouseButton(MouseButton),
  MouseMotion,
  MouseWheel,
}

impl From<KeyCode> for InputEvent {
  fn from(v: KeyCode) -> Self {
    Self::KeyCode(v)
  }
}
impl From<MouseButton> for InputEvent {
  fn from(v: MouseButton) -> Self {
    Self::MouseButton(v)
  }
}

#[derive(Clone)]
#[cfg_attr(feature = "serialize", derive(serde::Serialize, serde::Deserialize))]
pub struct InputMap<EV, CTX> {
  default_event_map: HashMap<InputEvent, Vec<EV>>,
  event_map: HashMap<CTX, HashMap<InputEvent, Vec<EV>>>,
}

impl<'a, EV, CTX> InputMap<EV, CTX>
where
  EV: Clone + Debug + Send + Sync + 'static,
  CTX: Eq + Hash + Clone + Debug + Send + Sync + 'static,
{
  pub fn new() -> Self {
    Self {
      default_event_map: HashMap::new(),
      event_map: HashMap::new(),
    }
  }

  pub fn add_event<RI>(&mut self, input_event: RI, game_events: Vec<EV>)
  where
    RI: Clone,
    InputEvent: From<RI>,
  {
    if let Some(events) = self
      .default_event_map
      .get_mut(&InputEvent::from(input_event.clone()))
    {
      events.extend(game_events);
    } else {
      self
        .default_event_map
        .insert(InputEvent::from(input_event), game_events);
    };
  }

  pub fn add_context_event<RI>(&mut self, ctx: CTX, input_event: RI, game_events: Vec<EV>)
  where
    RI: Clone,
    InputEvent: From<RI>,
  {
    let em = if let Some(em) = self.event_map.get_mut(&ctx) {
      em
    } else {
      self.event_map.insert(ctx.clone(), HashMap::new());
      self.event_map.get_mut(&ctx).unwrap()
    };

    if let Some(events) = self
      .default_event_map
      .get_mut(&InputEvent::from(input_event.clone()))
    {
      events.extend(game_events);
    } else {
      em.insert(InputEvent::from(input_event), game_events);
    }
  }

  pub fn run<T>(&self, input: &Input<T>, ctx: Option<&CTX>) -> Vec<EV>
  where
    T: Eq + Hash + Copy,
    InputEvent: From<T>,
  {
    let mut events = vec![];

    let _ = input
      .get_just_pressed()
      .map(|k| {
        // process global inputs
        if let Some(evs) = self.default_event_map.get(&InputEvent::from(*k)) {
          events.extend(evs.clone());
        }

        // process contextual inputs
        if let Some(ctx) = ctx {
          if let Some(em) = self.event_map.get(ctx) {
            if let Some(evs) = em.get(&InputEvent::from(*k)) {
              events.extend(evs.clone());
            }
          }
        }
      })
      .collect::<()>();

    events
  }

  // bevy system for processing input
  fn run_system<T>(
    im: Res<Self>,
    input: Res<Input<T>>,
    mut ev_writer: EventWriter<EV>,
    state: Option<Res<State<CTX>>>,
  ) where
    T: Eq + Hash + Copy + Send + Sync + 'static,
    InputEvent: From<T>,
  {
    let evs = im.run(&*input, None);
    //println!("Sending events: {:?}", evs);
    ev_writer.send_batch(evs.into_iter());

    if let Some(ctx) = state {
      let evs = im.run(&input, Some(ctx.current()));
      //println!("Sending ctx events: {:?}", evs);
      ev_writer.send_batch(evs.into_iter());
    }
  }
}

impl<EV, CTX> Plugin for InputMap<EV, CTX>
where
  InputMap<EV, CTX>: 'static,
  EV: Clone + Send + Sync + Debug + 'static,
  CTX: Eq + Hash + Clone + Debug + Send + Sync + 'static,
{
  fn build(&self, app: &mut AppBuilder) {
    app
      .add_event::<EV>()
      .insert_resource(self.clone())
      .add_system_set(
        SystemSet::new()
          .label("input")
          .with_system(Self::run_system::<MouseButton>.system())
          .with_system(Self::run_system::<KeyCode>.system()),
      );
  }
}

#[cfg(test)]
mod tests {
  use crate::*;

  #[test]
  fn sanity_check() {
    #[derive(PartialEq, Eq, Clone, Debug)]
    enum Events {
      Foo1,
      Foo2(i32),
    }
    #[derive(PartialEq, Eq, Clone, Hash, Debug)]
    enum Ctx {
      Ctx1,
      Ctx2,
    }

    let mut im = InputMap::new();
    im.add_event(KeyCode::A, vec![Events::Foo1]);
    im.add_context_event(Ctx::Ctx1, KeyCode::B, vec![Events::Foo2(5)]);
    im.add_context_event(Ctx::Ctx1, KeyCode::C, vec![Events::Foo2(6)]);
    im.add_context_event(Ctx::Ctx2, KeyCode::B, vec![Events::Foo2(-5)]);

    let mut input = Input::<KeyCode>::default();
    input.press(KeyCode::A);
    input.press(KeyCode::B);

    assert_eq!(vec![Events::Foo1], im.run(&input, None));
    assert_eq!(
      vec![Events::Foo1, Events::Foo2(5)],
      im.run(&input, Some(&Ctx::Ctx1))
    );
    assert_eq!(
      vec![Events::Foo1, Events::Foo2(-5)],
      im.run(&input, Some(&Ctx::Ctx2))
    );
  }
}
